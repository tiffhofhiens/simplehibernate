/* This program is a part of a simple hibernate example used for CIT-360
   It is written by Troy Tuckett.  BYUI.EDU

   Modified by Tiffany Quinlan 6/19/20
 */

import java.util.*;

public class RunHibernateExample {

    public static void main(String[] args) {

        TestDAO t = TestDAO.getInstance();

        List<User> c = t.getUsers();
        for (User i : c) {
            System.out.println(i);
        }

        System.out.println(t.getUser(1));
    }
}

