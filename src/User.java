/* This program is a part of a simple hibernate example used for CIT-360
   It is written by Troy Tuckett.  BYUI.EDU

   Modified by Tiffany Quinlan 6/19/20

 */

import javax.persistence.*;

/** This Data Object class corresponds with customer table
 *  in database. */
@Entity
@Table(name = "user")
public class User {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String toString() {
        return Integer.toString(id) + " " + name ;
    }
}